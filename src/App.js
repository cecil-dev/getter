import Header from './components/Header';
import SwipeCards from './components/SwipeCards';

function App() {
  return (
    <div className="App">
      <Header />
      <SwipeCards />
    </div>
  );
}

export default App;
