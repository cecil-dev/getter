import { useMemo, useState } from 'react';
import TinderCard from 'react-tinder-card';
import { createRef } from 'react';
import './SwipeCards.css';

import SwipeButtons from './SwipeButtons';
const stuntDoublesInitData = [
  {
    name: 'cecil',
    imagUrl:
      'https://static01.nyt.com/images/2020/05/16/business/16JORDAN-01sub/16JORDAN-01sub-mediumSquareAt3X.jpg',
  },
  {
    name: 'john',
    imagUrl: 'https://pbs.twimg.com/media/EVvK_g1XkAAX5qU.jpg',
  },
];
const alreadyRemoved = [];
const SwipeCards = () => {
  const [stuntDoubles, setStuntDoubles] = useState(stuntDoublesInitData);
  const childRefs = useMemo(
    () =>
      Array(stuntDoublesInitData.length)
        .fill(0)
        .map((i) => createRef()),
    []
  );
  const onSwipe = (direction) => {
    console.log('You swiped: ' + direction);
  };

  const onCardLeftScreen = (myIdentifier) => {
    console.log(myIdentifier + ' left the screen');
  };
  const swipe = (dir) => {
    alert(dir);
  };

  return (
    <div>
      <div className="card_container">
        {stuntDoubles.map((stuntDouble, key) => (
          <TinderCard
            key={key}
            onSwipe={onSwipe}
            onCardLeftScreen={() => onCardLeftScreen('fooBar')}
            preventSwipe={['up', 'down']}
            className="swipe"
          >
            <div
              style={{
                backgroundImage: `url(${stuntDouble.imagUrl})`,
              }}
              className="card"
            >
              <h3>{stuntDouble.name}</h3>
            </div>
          </TinderCard>
        ))}
      </div>

      <SwipeButtons
        swipeLeft={() => swipe('left')}
        swipeRight={() => swipe('right')}
      />
    </div>
  );
};

export default SwipeCards;
