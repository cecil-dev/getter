import './SwipeButtons.css';
import CloseIcon from '@material-ui/icons/Close';
import CheckIcon from '@material-ui/icons/Check';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  icon: {
    fontSize: 80,
  },
}));
const SwipeButtons = (props) => {
  const classes = useStyles();
  return (
    <div className="swipeButtons">
      <IconButton className="swipeBtnClose" onClick={props.swipeLeft}>
        <CloseIcon className={classes.icon} />
      </IconButton>
      <IconButton className="swipeBtnCheck">
        <CheckIcon className={classes.icon} onClick={props.swipeRight} />
      </IconButton>
    </div>
  );
};

export default SwipeButtons;
