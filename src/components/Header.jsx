import PersonIcon from '@material-ui/icons/Person';
import ForumIcon from '@material-ui/icons/Forum';
import './Header.css';

const Header = () => (
  <div className="header">
    <img
      className="headerLogo"
      src="https://www.nicepng.com/png/full/429-4290568_mission-impossible-iii-51838e834a4ce-mission-impossible-3-movie.png"
    />
  </div>
);

export default Header;
